'''
Faça um progrma que peça ao usuario para digitar um numero inteiro, informe se este numero é par
ou impar. Caso o usuário não digite um numero inteiro, informe que nao é um numero inteiro
'''

numero_inteiro = input('Digite um número inteiro: ')

if numero_inteiro.isdigit():
    numero_inteiro = int(numero_inteiro)

    if numero_inteiro % 2 == 0:
        print(f"{numero_inteiro} é número par!")
    else:
        print(f"{numero_inteiro} é número impar!")
elif not numero_inteiro.isdigit():
    print(f'{numero_inteiro} Isto não é um número inteiro!')

'''
Faça um programa que pergunte a hora ao usuário e, baseando-se no horário descrito, exiba a saudação apropriada.
Bom dia 0-11, Boa tarde 12-17 e Boa noite 18-23
'''
horario = input("Digite o horário (0 - 23): ")
if horario.isdigit():
    horario = int(horario)

    if horario < 0 or horario > 23:
        print('Horário de ter entre 0 e 23 horas!')
    else:
        if horario <= 11:
            print("Bom dia!")
        elif horario <= 17:
            print('Boa tarde!')
        else:
            print('Boa noite!')
else:
    print('Por favor, digite um horário entre 0 e 23 horas.')


'''
faça um programa que peça o primeiro nome do usuário. Se o nome tiver 4 letras ou menos escreva
"Seu nome é curto"; se tiver entre 5 e 6 letras, escreva "Seu nome é normal"; maior de 6 letras
escreva "Seu nome é muito grande".
'''
usuario = input('Qual o seu primeiro nome? ')
tamanho = len(usuario)
if tamanho <= 4:
    print("Seu nome de usuário é curto!")
elif tamanho <= 5 or tamanho <= 6:
    print('Seu nome é normal! ')
else:
    print('Seu nome é muito grande!')