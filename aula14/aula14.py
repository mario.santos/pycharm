'''
Ao escrever um código as vezes se reserva um lugar para incluir depois a função.
Se apenas deixar em branco e prosseguir o programa irá gerar um erro.
Para esse tipo de caso há duas opções o PASS e ...
Sendo o uso do PASS o correto
'''
valor = True
if valor:
    print("Oi!")
else:
    print("Tchau!")

valor2 = False
if valor2:
    print('Oi!')
else:
    print("Tchau!")

#utilizando o pass, mas poderia se substituido por ...
valor3 = True
if valor:
    #geralmente há comentarios explicando o que será feito neste local
    pass
else:
    print("Tchau!")
    #não apararecerá nada na tela qdo rodar.