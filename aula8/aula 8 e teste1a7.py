'''
*criar variaveis para nome(str), idade (int),
*Altura e peso (float)
*criar varialvel com ano atual (int)
*Obter o ano de nascimento da pessoa (usando como base o ano atual)
*Obter o IMC com duas casas decimais
*Exibir um texto com todos os valore na tela usando F-strings(com chaves)
'''
nome = 'Mario'
idade = 41
altura = 1.81
peso = 112.00
ano = 2022
nascimento = ano - idade
imc = peso / altura **2

print(f'{nome} tem {idade} anos, {altura} e pesa {peso} kg.')
print(f'O IMC de {nome} é {imc:.2f}.')
print(f'{nome} nasceu em {nascimento}.')
