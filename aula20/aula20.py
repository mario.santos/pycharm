#        Índices
#        0123456789....................33
frase = 'o rato roeu a roupa do rei de roma'
tamanho_frase = len(frase)
#não esquecer q o tamanho da frase é 34 pq ela conta o zero
#print(tamanho_frase)
contador = 0 #variavel de controle, a partir de onde começar
#print(frase[5]) #imprimindo uma caracter ja q a variavel tem indica

nova_str = '' #agora será copiado dados da variavel anterior e modificadas
# #1º crio uma 'nova_str'

#podemos interagir com o usuário e deixar ele escolher a parte da variavel q precisa usar/modificar
input_do_usuario = input('Qual letra deseja colocar maiuscula? ')
# no linha abaixo o q vem após <(limita até onde o while ocorrerá), posso colocar um nª representando o indice
#ou como esta abaixo toda a variavel.


while contador < tamanho_frase:
     #no exemplo abaixo vamos pegar uma letra especifica e modifica-la usando IF, 'r' para 'R'
     letra = frase[contador]
     if letra == input_do_usuario: #aqui dentro coloco a letra q quero alterar, ou assim como esta direciono para o usuario via input
          nova_str += input_do_usuario.upper() #aqui coloco o q deve vir no lugar q eu mandei alterar, neste caso .upper(maiusculo)
     else:
          nova_str += letra
     #print(frase[contador], contador)
     #nova_str += frase[contador]#aqui não estou somando, mas contatenando, copio integralmente str anterior.
     # a cada loop ele acrescentara uma letra da str original
     contador  += 1
     print(nova_str)



