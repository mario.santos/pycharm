'''
LEN QUANTIDADE DE CARACTERES
Conta quantidade de caracteres dentro de uma str, tb funciona com outros tipos.
APENAS NAO FUCIONA COM TIPOS NUMERICOS FLOAT E INT
'''
usuario = input("Digite seu nome de usuário: ")
qtidade_caracteres = len(usuario)
#para saber qtos caracteres tem e qual o tipo que retorna
print(usuario, qtidade_caracteres, type(qtidade_caracteres))

#mas como faço para limitar o numero de caracteres?
usuario = input("Digite seu nome de usuário: ")
qtidade_caracteres = len(usuario)
if qtidade_caracteres < 6:
    print("São necessário 6 caracteres para o cadastro do usuário.")
else:
    print("Usuário cadastrado com sucesso!")

'''
soma de caracteres
'''
string1 = input("digite alguma coisa: ")
string2 = input("digite outra coisa: ")

print(f'A quantidade total de caracteres digitados foram {len(string1) + len(string2)}')