'''
OPERADORES LOGICOS
IF ELIF e ELSE
usados para realizar duas comparações ou mais
AND
OR
NOT

IN
NOT IN
'''

a = 2
b = 2
c = 3
d = ''
e = 0
#and checara valores
print(a == b and b < c)
#or sempre precisa que um seja verdadeiro
print(a == b or b < c)
#para inverter expressoes use not
print(not a == b and not b < c)
if not d:
    print('Por favor preencha o valor de d!')
# se colocarmos o e que é igual a zero, tb dara mesma resposta pq se trata de um booleano falso
if not e:
    print('Por favor preencha o valor de e!')

#pesquisa se algo existe na variavel informada
nome = 'Mario'
if "i" in nome:
    print('Existe a letra I no seu nome.')
else:
    print('Não existe!')

# o not in inverterá a expressão do in...mesmo a letra I existindo no nome ele dirá que não há
'''outra coisa possivel é que se um determinado caracter ou informação exister na informação executar
uma determinada função ou resposta, caso não tenha executar outra'''
nome = 'Mario'
if "i" not in nome:
    print('Existe a letra I no seu nome.')
else:
    print('Não existe!')
informacao = 'yes'
if "yes" not in informacao:
    print('Prosseguir')
else:
    print('retornar ao inicio')

'''
EXERCICIO BASICO DE CHECAGEM DE USUARIO E SENHA
'''
usuario = input('Nome de usuário: ')
senha = input('Senha do usuário: ')

bc_dados_usuario = 'SantosMario'
bc_dados_senha = '54321'

if bc_dados_usuario == usuario and bc_dados_senha == senha:
    print("Seja bem vindo, você esta logado")
else:
    print('Senha incorreta!')
