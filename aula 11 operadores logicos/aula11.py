'''
os operadores relacionais são feitos justamente para fazer comparações entre coisas.
sempre que forem usados expressões INTEIRAS ira responder com um valor BOOLEANO
QUANDO USAR UM SINAL DE = ESTA AFIRMANDO QUE UMA COISA É IGUAL A OUTRA
QUANDO USAR DOIS SINAIS DE == ESTA PERGUNTANDO SE UMA COISA É IGUAL A OUTRA
'''
# = =
print(2==2)
#esta perguntando se dois é igual a dois
num_1 = 2
num_2 = '2'
num_3 = 2
num_4 = 3
#print(num_1==num_2)
expressao = num_1==num_3
#print(expressao)

''' 
> este operador é MAIOR QUE
'''
exmaiorq = num_1 > num_3
#print(exmaiorq)
ex2maiorq = num_4 > num_1
#print(ex2maiorq)

'''
>= este operador checa se o valor da esquerda é maior ou igual o da direita  
'''
#print(num_1 >= num_3)
#print(num_1 >= num_4)

'''
< este operador verifica se é menor que
'''
#print(num_1 < num_4)

'''
< este operador verifica se é menor que ou igual 
'''
#print(num_4 <= num_3)

'''
ESTAS OPERAÇOES TB FUNCIONAM COM PALAVRAS
se fazer a operação colocando um ponto de exclamação (!) antes do igual
voce estará perguntando se são diferentes (!=)
exemplo abaixo pergunto se num_1(2) é diferente de num_3(2)
'''
#print(num_1 != num_3)
#agora vou perguntar se num_1(2) é diferente que num_2('2') str
#print(num_1 !=num_2)

'''EXERCICIO
DESCOBRI NOME DO USUARIO
SE É MAIOR DE IDADE
SE PODE RETIRAR UM EMPRESTIMO
'''
nome = input('Qual é seu nome? ')
idade = input('Qual é sua idade? ')
#como a idade é um input será retornada como str, precisa ser convertida em int
idade = int(idade)
#idade limite para pegar emprestimo
muito_jovem = 20
muito_velho = 65
if idade >= muito_jovem and idade <= muito_velho:
    print(f'{nome} teve o emprestimo concedido.')
else:
    print(f'{nome} imprestimo não concedido.')

