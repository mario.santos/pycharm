'''
condições if, elfi e else
'''



if True:
    print('verdadeiro.')

    num_1 = 2
    num_2 = 4

    print(num_1 + num_2)


if True:
    print("verdadeiro.")
else:
    print("Ñ é verdadeiro.")
#abaixo exemplo se substuituirmos o true(verdadeiro)  por false(falso)

if False:
    print("verdadeiro.")
else:
    print("Ñ é verdadeiro.")
if False:
    print('verdadeiro.')
elif True:
    print("agora é verdadeiro.")

#agora usando mais de um elif para verificar, podem ser usandos qtos elif forem necessários
if False:
    print("verdadeiro.")
elif False:
    print("Agora é verdadeiro.")
if True:
    print('verdadeiro texte com mais de um elif.')
elif False:
    print("Mais uma verdadeira.")
else:
    print("Não é verdadeiro.")

#caso nao tenha nunhuma expressão verdadeira o else ser printado
if False:
    print("verdadeiro.")
elif False:
    print("Agora é verdadeiro.")
if False:
    print('verdadeiro texte com mais de um elif.')
elif False:
    print("Mais uma verdadeira.")
else:
    print("Não é verdadeiro qdo todas as expressões são false, dai o else entra em ação.")
#agora um exemplo mais pratico do uso, com input
if False:
    print("verdadeiro.")
    print("teste teste2")
    #não vai rodar nada pq é false
elif True:
    print("Agora é verdadeiro.")
    nome = input('Qual é o seu nome?' )
    print(f'O nome do usuário é {nome}.')
if False:
    print('ñ ira rodar pq é false, mas mesmo se fosse verdadeiro ñ rodaria pq a expressão de cima já foi rodade por ser verdadeira.')
elif False:
    print("Mais uma falsa.")
else:
    print("caso todas as demais fossem falsas, o else seria executado.")
