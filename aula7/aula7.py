nome = 'mario'
idade = 42  #int
altura = 1.81  #float
e_maior = idade > 18 #bool
peso = 112
imc = peso / altura **2

print(nome, 'tem', idade, 'anos de idade e seu IMC é', imc)
print(f'{nome} tem {idade} anos de idade e seu imc é {imc}')
print(f'{nome} tem {idade} anos de idade e seu imc é {imc:.2f}')
#outra forma de formatar
print('{} tem {} anos de idade e seu imc é {:.2f}'.format(nome, idade, imc))
print('{2} tem {0} anos de idade e seu imc é {1}'.format(nome, idade, imc))
''' troca as variaveis por numero e coloca os numeros onde quer que elas repitam'''
'''essa função me permite renomear as variaveis segue exemplo'''
print('{n} tem {i} anos de idade e seu imc é {im:.2f}'.format(n=nome, i=idade, im=imc))
