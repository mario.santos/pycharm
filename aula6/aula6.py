'''variaveis são um apelido p algo salvo na memória da mq
sempre iniciam com letras
qual usar uma variavel na verdade estará usando a variavel dentro dela'''
nome = 'mario'  #isso se chama um operador de atribuição
print(nome, type(nome)) #agora nome esta ligado a mario
idade = 42  #int
altura = 1.81  #float
e_maior = idade > 18 #bool
peso = 112
imc = peso / altura **2
print('Nome:', nome)
print('Idade:', idade)
print('Altura:', altura)
print('É de maior:', e_maior)
print(peso)
print(imc)

print(nome, 'tem', idade, 'anos de idade e seu IMC é', imc)