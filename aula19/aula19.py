''' WHILE / ELSE
                              CONTADORES
O laço while precisa de um númer q no caso é o contador, ele garante que o laço tenha um fim
o número é um limitador.
IMPORTANTE, esse número escolhido sempre deve ser menor que o parametro limitador
ex: contador = 50
while contador < 100(parametro limitador)
    print(contador) irá imprimir até o numero 99, se precisar que apareceça até o 100 fazer(numero <=100)
    contador = contador + 1
é um contador linear, contará o intervalo de números que indicar
'''


                             # ACUMULADORES
contador = 1
acumulador = 1 #valor que será somado a cada laço, acrescenta a conta não linearmente

while contador <= 10: #parametro limitador, aqui estou limitando apenas o contador, o acumulador não)
    print(contador, acumulador) #no primeiro laço ambos serão um,
    #irá imprimir até o numero 9, se precisar que apareceça até o 10 fazer(numero <=10)

    acumulador = acumulador + contador #dentro do laço while chamo acumulador+contador, fara q em cada loop
                                    #some o acumulador + o laço passado, gerando nºs exponenciais
    contador = contador + 1
#NO WHILE DA P USAR O ELSE VEJA COME NA SEQUENCIA DO EXEMPLO
else: #else entra em ação qdo o limitador para o loop, ou seja, qdo o while deixa de ser true
    print('Final do exemplo, chegamos no ELSE!') #se usar um BREAK ANTES, não executara o ELSE
    # no caso de BREAK deve se usar um print normal, fora do else no final do código