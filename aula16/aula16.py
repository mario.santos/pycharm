'''
Formatando valores com modificadores
:s - Texto (string)'''
nome = 'Mário Santos'
#o "s" só esta dizendo que é uma string
print(f'{nome:s}')

''':d - Inteiros (int)'''

''':f - Números de ponto flutuante (float)'''
#por exemplo vamos dividir 10/3, dara uma disma periódica, iremos reduzir a resposta a duas casas
#o "f" dentro do print se refere ao numero flutuante q será a resposta
#primeira maneira
num_1 = 10
num_2 = 3
divisao = num_1 / num_2
print(f'{divisao:.2f}')
#segunda maneira
print( '{:.2f}'.format(divisao))

''': (caracatere)(> ou < ou ^ ) (quantidade)(tipo - s, d ou f)'''
'''> esquerda; < direita; ^ centro'''
#neste aqui podemos dizer que nossa str irá ter um tamanho padrão
#mas se eu quiser q essa variavel tenha 10 casas, mas quero que as casas sejam preenchidas com zeros
num_3 = 1
print(f'{num_3:0>10}')
#estou dizendo que quero que meu nº seja preenchido com zeros a esquerda,quero 10 casas
#não quer dizer que os 10 zeros aparecerão, mas que terá no maximo 10 casas caso apareçam
num_4 = 1914
print(f'{num_4:0>10}')#nome que terá mesmo numero de casas, 10 apenas serão completadas com zeros
# se quiser que o zeros troquem de possição apenas troco os sinas ><
print(f'{num_4:0<10}')
# se quiser que o nº da variavel fique no centro dos zeros coloco ^
print(f'{num_4:0^10}')
#posso usar a formatação para transformar um nº inteiro em float
print(f'{num_4:.2f}')
#posso combinar as funções transfomando em float e incluindo casas decimais
print(f'{num_4:0>10.2f}')#note que ele continua mesmo com o float tendo apenas 10 casas como orientado
#isso funciona com str tb
print(f'{nome:*^20}')#neste caso disso que quero o nome com 20 casas e centralizado entre os *
#lembrando que se voce precisar saber qtos caracteres tem a variavel pode usar o 'len'
print(len(nome))
#ele faz a conta de qtos * deve por se eu quiser
print(20 - len(nome))
#tb me diz qtos * ficaram de cada lado do nome
print((20 - len(nome)) / 2)

'''AGORA VAMOS USAR A FUNÇÃO NOME FORMATADO'''
#as duas chaves estão representado a variavel nome
nome_formatado = '{}'.format(nome)
print(nome_formatado)
#se quiser usar os modificadores na variavel, coloco dentro das chaves a informação
nome_format = '{:@^25}'.format(nome)
print(nome_format)
#se mandar ter o mesmo nº de caracteres q a str já tem, não add nada

"""EXISTEM VARIAVEIS NOMEADAS"""
'''Nelas é possivel repetir a variavel sem ficar escrevendo o nome dela'''
nome_format = '{n} {n} {n} {n}'.format(n=nome) #note q nome agora é "n" e poderei repeti-la só colocando esta letra
print(nome_format)
#pode ser formatada normalmente como antes
nome_format1 = '{n:#<20}'.format(n=nome)
print(nome_format1)

'''INDICES'''
#Caso eu tenha varias variaveis poderia fazer um indice
#exemplo com nome sobrenome e idade
sobrenome = 'Dalvan'
idade = '41'
nome_format2 = '{1}'.format(nome, sobrenome, idade) #lembrando q 1ª colocação é zero
print(nome_format2) #mandado imprimir por indice a posição 1-sobrenome
#posso formatar ele com mesmas funções já mostradas, com numeros de casas ou caracteres dentro {}
#caso queira colocar mais uma informação no indice, acrescentar mais uma {}, com formatação se quiser
nome_format3 = '{1:*<10}{0:#<20}{2:@^6}'.format(nome, sobrenome, idade)
print(nome_format3)

#em uma str se colocar ponto no final da variavel aparecerá varias opçoes de formatação
#variavel.zfill(preenche com zeros até o numeros de casas que mandar
esposa = 'rakel'
esposa = esposa.zfill(10)
print(esposa)
#variavel.ljust preenche a direita com caracates que informar
wife = 'kel'
wife = wife.ljust(20,'¢')
print(wife)
print(nome.lower()) #tudo minusculo
print(sobrenome.upper()) #tudo maiusculo
print(nome.title()) #primeiras letras maiusculas

#FATIAMENTO DE STRS
nome_format5 = (nome, sobrenome, idade)[1:3]
print(nome_format5)