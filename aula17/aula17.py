'''INDICES E FATIAMENTO DE STRS
Manipulação de strs'''
#STRINGS INDICES
#cada caracter da variavel texto tem um indice, incluive o espaço, um nº q o representa
#positivos+[012345678]
texto = 'Python_s2'
print(texto[2]) #imprimo apenas a letra 't'
#negativos-[987654321] nos negativos não inclui zero, ele é positivo é o primeiro
print(texto[-4]) #impresso de tras p frente


#FATIAMENTO DE STRINGS [inicio;fim;passo]
#com ele informo o interpretador de onde até onde quere q ele busque a informação
#na variavel texto vamos usar as letras 'thon'
print(texto[2:6]) #sempre colocar o indice 1 nº a mais, o ultimo não é incluido
#se quiser q comece o fatiamento do primeiro caracter ñ precisa colocar o [:6],o mesmo se quiser até o ultimo.
print(texto[:6])
print(texto[-8])
print(texto[-9:-3])
#se desejar que ele leia pulando um caracter usar :2, o nº q acrescentar depois do :, é qtidade casa q ira pular
print(texto[0::2])
#tb posso dizer o intervalo, até onde ele pode ler pulando as casas, ex: vai ler da zero a seis pulando 1 casa
print(texto[:6:2])
print(texto[0::3]) #como não coloquei até onde ir, irá até o final lendo pulando 2(3 pq ele não le ultimo nº) casa


#FUNÇÕES BUILT-IN LEN, ABS, TYPE, PRINT, ETC...essas funcões pode ser usadas diretamente em cada tipo
print(len(texto)) #informa qtos caracteres tem a variavel
#para imprimir cada letra da minha str em uma linha
for letra in texto:
    print(letra)