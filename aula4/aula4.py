# tipos de dados
# str - string = textos, basicamente coisas que estao entre aspas
#int - inteiro= numeros positivos ou negativos incluindo o zero S/ ponto
#float - real/ponto flutuante = numeros positivos e negativos incluindo o zero COM ponto. No python nao se usa virgula para separar se usa ponto
#bool - booleano/logico = soh possui dois valores TRUE OU FALSE, sao valores logicos pq checam os outros dados, usados geralmente em comparacoes.
print('mario', type('mario'))
print(10, type(10))
print(10.42, type(10.42))
print(10==10, type(10==10))#serve tb com letras
# '''isso mostrara que tipo de dados se refere dentro do print, pode ser usado com qualquer tipo de comando mostra a q classe/comando o dado pertence'''

print('mario', type('mario'), bool('mario'))
print('10', type('10'), type(int('10')))# foi dito p ele pegar um str dizer a classe e converter em inteiro. Se apenas colocasse o ultimo 10 sem aspas ele ja seria um inteiro. Nem sempre eh possivel, nao se converte texto em numero por exemplo. Numero de ponto float nao converte em numero inteiro. Mas numero inteiro PODE SER convertido em float


#exercicio
#string nome
print('mario', type('mario'))
#int idade
print(41, type (41))
#float altura
print(1.81, type (1.81))
#booleano verificar se eh maior de idade, para isso usa o sinal de >
print(41 > 18, type(41 > 18))