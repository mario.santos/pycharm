'''
precisamos as vezes interagir com o usuaria, pedir dados.
Para isso o python tem uma ferramenta chamada input
'''
nome = input("qual é o seu nome? ")
idade = input("qual é a sua idade? ")
print(f'O usuário digitou {nome} , ele tem {idade} anos. O tipo da variavel é {type(nome)}')
#as informações do input sempre retornam com str
#veja como transforma-los para poder usar os dados em somas por exemplo
ano_nascimento = 2022 - int(idade)
print(f'{nome} tem {idade} anos, nasceu em {ano_nascimento}.')

numero1 = input('digite um numero')
numero2 = input('digite outro numero')
print(int(numero1) * int(numero2))
