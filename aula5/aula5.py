'''
operadores aritimeticos
+ adicao
- subtracao
* multiplicao
/ divisao
// divisao inteira, resultado da divisao eh arredondado
** exponeciacao, numero elevado a outro, potenciacao
% resto da divisao, NAO EH PORCENTAGEM, retorna o modulo, resto da divisao entre um numero e outro.
() sao usados para alterar a precedencia das contas
'''
print('multipicão *',10*10)
print('mutiplicação com strg * int', 10 * '10')  #o operador de  multiplicação tb serve como operador de repetição

print('adição +',10+10) # + alem de soma faz contanecação
print ('adição de duas str ele junta os numeros, mas não posso juntar int+str', '5'+'5')
print('somar strg palavras', 'mario'+' '+'santos e ele tem ' + str(42)  +  ' anos')

print('subtração -',10-5)

print('divisão /', 10/2)
print('divisão inteira posso usar ponto flutuante e nº inteiro, ñ pode ser usado com str', 10.5 // 3)
#colocou como resultado nº inteiro pq com duas // aredondoa, mas se fizer com apenas uma / e ponte flutuante muda
print(10.5 / 3)

print(('potenciação qdo coloca nº com dois **', 2**10))

print('a % mostra o modulo, resto da divisão', 3%15)

print('() são utilizados para alterar a procedencia dos operadores como feito no exemplo de soma str palavra acima')
print(5+2*10)  #as operações sempre efetuarão o q esta dentro do parenteses primeiro, se estiver tudo junto primeiro será executada a multiplicação.
print((5+2)*10)