'''
FOR in em Python
Iterando strins com for
Função range(start=0, stop, step=1)
'''

'''
no laço while se faz assim:
texto = 'python'

contador = 0
while contador < len(texto):
    print(texto[contador]
    c +=1
'''
# no laço FOR é+ simples, não precisa de contador...veja:
#sabendo que a str é finita

texto = 'python'
print('usando função enumerate, exemplo for')
for n, letra in enumerate(texto): #faz exatamente igual o while mas de forma mais simples
#                     #caso queira colocar o numero de laços usar a função enumerete. Se tirar o 'texto' dos (),
#                     # i n, e a função imprimirá apenas a str
     print(n, letra)
print('agora para alterar, para maiuscula letras dentro de uma variavel, lembrando q não podemos alterar a variavel é necessario copiar, como vimos na aula 20, veja como fazer com laço FOR')
#cria nova_str
nova_str = ''
for letra in texto:
    if letra == 't':
        nova_str = nova_str + letra.upper()
    elif letra == 'h':
        nova_str = nova_str + letra.upper()
    else:
        nova_str = nova_str + letra
print(nova_str)
'''aqui é possivel utilizar o CONTINUE E O BREAK visto anteriormente'''
'''CONTINUE pula para o proximo laço, por exemplo poderia colocar um CONTINUE após o IF, iria pular...'''
'''BREAk para de executar o laço, p exemplo se eu quiser parar o laço poderia colocar o BREAK onde achar q precisa'''

print('Abaixo prints da função RANGE')

'''               FUNÇÃO RANGE         
recebe 3 argumentos
start=0, stop, step=1
'''
#esse 'n' é qualquer variavel
                           # START aqui o 5 é o argumento de inicio, o padrão é zero
                           # STOP aqui o 35 é o arguemento de parada, até onde irá, nunca aparece o nª
                           # STEP aqui o 3 é o intervalor de soma a cada laço,
                           # de quantos em quantos casas fazer, padrão UM. Caso precise q seja decrescente
                           # usar o STEP NEGATIVO, serve tb para achar os multiplos do nº q esta STEP'''
#for n in range (5, 35, 3):
#    print(n)

#para achar multiplos, nesse caso de 100, só coloquei nº final e usei os outros no padrão star e step
#lembrando q % se refere ao resto/resultado da divisão
for n in range(100):
    if n % 8 ==0:
        print(n)