'''
 LISTAS EM PYTHON

variavéis são uma forma de quardar valores na memória
tipos de variavéis, bool, str, int e flout
LISTA É UM TIPO DE VARIAVEL, nela pode haver vários valores de diferentes tipos.
lista é guardada em colchetes e é criada como uma variavel normal, dando o nome
para acessar os valores da lista uso indice, lembrando que começa no zero, e de trás p frente ficam negativos
Dentro de cada indice posso colocar varios valores, diferente da strg q era apenas um valor
se quiser achar o ultimo valor da lista e não sabe qual é, basta colocar -1 no indice
'''
lista = ['A', 'Banana', 'C', 'D', 'E', 10.5, 22]
print(lista[1]) #acessando valor lista por indice
#para modificar um valor da lista, inclusive de int para str
lista [-1] = "vinte e dois."
print(lista)


print('FATIAMENTO')
'''lembrando que o fatiamento suporta 3 passos [pode começar do zero: vai até onde quiser: e da pulos]
no caso do exemplo começa no zero: vai até a 5 e pula de 3 em 3 casa
lembrando que o valor da casa final no fatiamento não aparece, para sempre uma casa antes[0:este aqui:1]
'''
print(lista [0:5:3])
#para inverter correr valores de trás para frente, digamos q é do 1º ao ultimo, apenas na ordem contraria
#pode usar isso numa str tb
print(lista[::-1])

print('APPEND - inserir valores no final da lista')
l1 = [1, 2, 3]
l2 = [4, 5, 6]
l3 = l1 + l2
print(l1)
#print(l2)
#print(l3)
l3.append('X') #add a lista outro valor, isso poderia ser feito a qualquer outra lista.
#append insere no final da lista
print(l3)
# se quiser posso acessar agora o valor incluido, mandando imprimir por indice
print(l3[6])

print('INSERT - inserir valores em qq lugar da lista')
#nele preciso dizer em qual indice quero incerir e qual valor.
l2.insert(0, 88)
print(l2)

print('POP - remover o ultimo elemento da lista')
l4 = [93, "banana", 8.1, 3, 5]
print(l4)
l4.pop()
print(l4)

print('DEL - deletar um elemento ou uma fatia de elementos da lista')
#no exemplo vamos excluir os indices 1 e 2, o 3 não pq o ultimo ele nunca pega é até ali
del(l4[1:3])
print(l4)

print('CLEAR - limpar a lista')

print('EXTEND - juntar duas listas (função parecida com append)')

l1 = [1, 2, 3]
l2 = [4, 5, 6]
l1.extend(l2)
#apenas a l1 adicionou l2 continua igual
print(l1)
#print(l2)
#posso add apenas um valor a lista tb, q não esta em outras listas
l2.extend('%')
print(l2)

#MIN E MAX
#pegar o maior valor da lista,mesmo o 54 sendo o ultimo o maior valor é 81
l5 = [0, 22, 10.1, 81, 7, 26, 54]
print(l5)
print(max(l5))
print(min(l5))

print('RANGE')
#como criar uma sequencia de numeros progressivos, sem precisar digitar todos
#importante que o range não gera uma lista, ele cria objetos. Por isso precisa ser transformado em lista0
#l6 = list(range(1,10)) #caso queira que ele pulo casa só acrescentar depois do dez o intervalo
l6 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
print(l6)
soma = 0
#transformando em lista podemos trabalhar com os valores
for valor in l6:
    soma = valor + soma #soma de todos os valores

print(soma)
#para checar elementos de uma lista com varios elementos diferentes, de que tipo são
l7 = [10.10, "string", -100, False]

for elem in l7:
    print(f' O tipo de elem é {type(elem)} e seu valor é {elem}')
print()
print()
print('EXERCÍCIO JOGO DA FORCA')
secreto = "CASAMENTO"
digitadas = []
chances = 5

while True:
    if chances <= 0:
        print("Você perdeu!!!")
        break

    letra = input("Digite uma letra: ")

    if len(letra) > 1:
        print(' Ahhh isso não vale, digite apenas uma letra.')
        continue

    digitadas.append(letra)

    if letra in secreto:
        print(f'Uhuuulll, a letra"{letra}" existe na palavra secreta.')
    else:
        print(f' Há nãoooo: a letra {letra}" NÃO EXISTE na palavra secreta.')
        digitadas.pop()

    secreto_temporario = ""
    for letra_secreta in secreto:
        if letra_secreta in digitadas:
            secreto_temporario = secreto_temporario + letra_secreta
        else:
            secreto_temporario += "?"

        if secreto_temporario == secreto:
            print(f'Que legal, VOCÊ VENCEU!!! A palavra secreta era {secreto_temporario}.')
            break
        else:
            print(f'A palavra secreta está assim: {secreto_temporario}.')

        if letra not in secreto:
            chances -= 1

        print(f' Você ainda tem {chances} chances.')
        print()