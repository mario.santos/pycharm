'''
WHILE EM PYTHON
Utilizado para realização de ações enqto uma condição é verdadeira(repetições)
Gera um loop
usa condições e operações
'''
#while True: #se eu colocar que essa condição é verdadeira, ela sempre será até q eu enterrompa
    #gera loop infinito, é um circulo no código... até interrupção
#    nome = input('Qual seu nome: ')#irá ter um código filho, cria um recuso só p códigos dele
#    print(f' Olá {nome}!')
#print('Não será executado até que será false')

#agora vamos fazer uma contagem até 10 na tela
#x = 0
#while x < 10:
#    print(x)
#    x =  x + 1 #precisarei mudar o valor da expressão pq ele vai ler q x é menor q zero e repetira sempre
#    #preciso informar qdo ele deve para com o loop, aparece 9 pq x passou a ser igual a 10

#print("Acabou!")

#junto com o while pode se usar o comando CONTINUE, ele fará sair do loop e ir p próxima tarefa.
#x = 0
#while x < 10:
#    if x == 3: #nesse caso tomar cuidado, qdo ele chegar no 3 irá parar, ñ dará sequencia, terá q ajustar comando.
#        x = x + 1 #não irá executar x = x + 1 no final do código, preciso repetir comando aqui, ele fica com o x sempre em 3
#        continue #note no resultado que pulou o numero tres como mandando

#    print(x)
#    x =  x + 1
#print('Acabou!')


#junto com o while pode se usar o comando BREAK, ele fará ela finaliza o loop e acaba o código
#x = 0
#while x < 10:
#    if x == 3:
#        x = x + 1
#        break #note que apesar do código ter mais coisas a executar, ele parou aqui

#    print(x)
#    x =  x + 1
#print('Acabou!')


'''PENSE EM LAÇOS DE REPETIÇÃO COMO SE FOSSE UM CIRCULO DENTRO DO SEU CÓDIGO ONDE PODE ESCREVER MAIS 
CÓDIGOS DENTRO DELE'''
#POR exemplo veja nesse exemplo como dentro de cada loop de x acontece 5 loops de y

#x = 0 #coluna

#while x < 10: #enqto x for menos que 10
#    y = 0  # linha, o Y Precisa ser zerado a cada volta do loop se nao não funciona como esperado, por isso fica dentro while do X
#    while y <5: #aqui criei outro bloco de while dentro do já existe para checar y, q ira rodar dentro o x
#        print(f'X vale {x} e Y vale {y}')
#        print(f'({x},{y})') #ira imprimir o mesmo resultado como coordenadas
#        y = y + 1

#    x = x+1 #ou x+=1

#print('Acabou!')


'''COM WHILE PODEMOS MONTAR UMA CALCULADORA BASICA DE 4 FUNÇOES'''

while True:
    print()
    num_1 = input("Digite o nº desejado: ")
    num_2 = input("Digite o 2º nº da operação: ")
    operador = input("Digite a operação matemática: ")
    #sair = input("Deseja sair? [s]im ou [n]ão: ")
    #if sair == 's':
    #    break
    if not num_1.isnumeric() or not num_2.isnumeric(): #caso se digite outro caracter ñ nº
        print("Você precisa digitar um número válido!")
        continue

    num_1 = int(num_1) #para transformar as str em int
    num_2 = int(num_2)

    if operador == '+':
        print(num_1 + num_2)
    elif operador == "-":
        print(num_1 - num_2)
    elif operador == "/":
        print(num_1 / num_2)
    elif operador == "*":
        print(num_1 * num_2)
    else:
        print("Operador inválido!")
    sair = input("Deseja sair? [s]im ou [n]ão: ")
    if sair == 's':
        break